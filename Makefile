.PHONY: docs check mypy pylint format build deploy deploy_test
.SILENT: deploy deploy_test # do not echo commands with password

# generate documentation
docs:
	poetry run sphinx-apidoc -o docs . && poetry run sphinx-build -b html docs public

# check
check:
	pre-commit run --all-files

# check static typing
mypy:
	pre-commit run --all-files mypy

# check code errors
pylint:
	pre-commit run --all-files pylint

# format code
format:
	pre-commit run --all-files black

# build a wheel package in dist folder
build:
	poetry build

# upload on PyPi repository
deploy:
	poetry publish --build

# upload on PyPi test repository
deploy_test:
	poetry config repositories.pypi_test https://test.pypi.org/legacy/
	poetry publish --build --repository pypi_test
