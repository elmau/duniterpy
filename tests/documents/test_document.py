# Copyright  2014-2022 Vincent Texier <vit@free.fr>
#
# DuniterPy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DuniterPy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pytest

from duniterpy.constants import G1_CURRENCY_CODENAME, G1_TEST_CURRENCY_CODENAME
from duniterpy.documents.document import Document

SIGNATURE = "sig"
DOC_REFERENCE = (10, G1_CURRENCY_CODENAME)


def test_document_equality():
    doc1 = Document(*DOC_REFERENCE)
    doc1.signature = SIGNATURE
    doc2 = Document(*DOC_REFERENCE)
    doc2.signature = SIGNATURE
    assert doc1 == doc2


@pytest.mark.parametrize(
    "doc_kwargs, signature",
    [
        ((42, G1_CURRENCY_CODENAME), None),
        ((10, G1_TEST_CURRENCY_CODENAME), None),
        (DOC_REFERENCE, SIGNATURE),
    ],
)
def test_document_inequality(doc_kwargs, signature):
    doc1 = Document(*DOC_REFERENCE)
    doc2 = Document(*doc_kwargs)
    if signature:
        doc1.signature = signature
        doc2.signature = signature[::-1]
    assert not doc1 == doc2
